package EX6_Flowers;

// Создаём букет цветов
public class Kit {
    public static Flower rose1 = new Flower("Rose1", "red", 20, 10);
    public static Flower rose2 = new Flower("Rose2", "red", 20, 9);
    public static Flower rose3 = new Flower("Rose3_Vyalaya", "white", 3, 18);

    public static Flower lily1 = new Flower("Lily1", "red", 24, 20);
    public static Flower lily2 = new Flower("Lily2", "yellow", 44, 23);
    public static Flower lily3 = new Flower("Lily3", "red", 24, 20);

    public static Flower acacia1 = new Flower("Acacia1", "white", 7, 24);
    public static Flower acacia2 = new Flower("Acacia2", "children's surprise", 9, 19);
    public static Flower acacia3 = new Flower("Acacia3", "white", 8, 25);

    public static Flower cactus1 = new Flower("Cactus1_undead", "green", 100500, 37);
    public static Flower cactus2 = new Flower("Cactus2", "green", 154, 64);
    public static Flower cactus3 = new Flower("Cactus3", "pink", 147, 31);
}