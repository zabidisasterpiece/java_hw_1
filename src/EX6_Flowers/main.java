package EX6_Flowers;

import java.util.ArrayList;
import java.util.HashSet;

public class main {
    public static void main(String[] args) {
        Kit flowers = new Kit();
        System.out.println(); //Для удобства чтения отчёта

        int allCosht = 0;
        ArrayList<Integer> allTimeDead = new ArrayList(); // Сюда засовываем все показатели "живучести" цветов
        HashSet<String> allColors = new HashSet<>(); // Сюда засовываем все цвета

        allCosht = flowers.rose1.cosht + flowers.rose2.cosht + flowers.rose3.cosht + flowers.lily1.cosht + flowers.lily2.cosht + flowers.lily3.cosht
                + flowers.acacia1.cosht + flowers.acacia2.cosht + flowers.acacia3.cosht
                + flowers.cactus1.cosht + flowers.cactus2.cosht + flowers.cactus3.cosht;
        System.out.println("Цена за букет " + allCosht + " bucks"); //  Общая цена

        allColors.add(flowers.rose1.color);
        allColors.add(flowers.rose2.color);
        allColors.add(flowers.rose3.color);
        allColors.add(flowers.lily1.color);
        allColors.add(flowers.lily2.color);
        allColors.add(flowers.lily3.color);
        allColors.add(flowers.acacia1.color);
        allColors.add(flowers.acacia2.color);
        allColors.add(flowers.acacia3.color);
        allColors.add(flowers.cactus1.color);
        allColors.add(flowers.cactus2.color);
        allColors.add(flowers.cactus3.color);
        System.out.println(allColors + "    Всего цветов " + allColors.size());

        allTimeDead.add(flowers.rose1.deadTimeHour);
        allTimeDead.add(flowers.rose2.deadTimeHour);
        allTimeDead.add(flowers.rose3.deadTimeHour);
        allTimeDead.add(flowers.lily1.deadTimeHour);
        allTimeDead.add(flowers.lily2.deadTimeHour);
        allTimeDead.add(flowers.lily3.deadTimeHour);
        allTimeDead.add(flowers.acacia1.deadTimeHour);
        allTimeDead.add(flowers.acacia2.deadTimeHour);
        allTimeDead.add(flowers.acacia3.deadTimeHour);
        allTimeDead.add(flowers.cactus1.deadTimeHour);
        allTimeDead.add(flowers.cactus2.deadTimeHour);
        allTimeDead.add(flowers.cactus3.deadTimeHour);

        allTimeDead.sort(null);

        System.out.println(allTimeDead + " количество элементов " + (allTimeDead.size()-1)); // Для контроля
        System.out.println("Букет завянет через " + allTimeDead.get(allTimeDead.size()-1) + " часов ( часа / час )!");
    }
}