package exam;

import java.util.Scanner;

public class ex1 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Введтите число!");
        Scanner scanner = new Scanner(System.in);
        int num = in.nextInt();
        int sec = num % 60;
        int minutes = (num - sec) / 60;
        int day = 0;
        int week = 0;
        int hours = 0;
        int hours_2 = 0;

        String text_1_week = " неделя ";
        String text_week = " недели ";
        String text_more_week = " недель ";

        String text_1_day = " день ";
        String text_days = " дня ";
        String text_more_days = " дней ";

        String text_1_hour = " Час ";
        String text_hours = " Часа ";
        String text_more_hours = " Часов ";

        String text_1_min = " Минута ";
        String text_minutes = " Минуты ";
        String text_more_minutes = " Минут ";

        String text_1_sec = " секунда ";
        String text_sec = " секунды ";
        String text_more_sec = " секунд ";

        String text_for_sec = " sec ";
        String text_for_min = " min ";
        String text_for_hour = " hour ";
        String text_for_day = " day ";
        String text_for_week = " week ";

        String text_finish = new String();

        //начинаем вычисленя до недели
        if (minutes > 59) {
            minutes %= 60;
            hours = (num - 60 * minutes - sec) / 3600;
            if (hours > 23) {
                hours_2 = hours % 24;
                day = (hours - hours_2) / 24;
                hours = hours_2;
                if (day > 6) {
                    int day_2 = day % 7;
                    week = (day - day_2) / 7;
                    day = day_2;
                }
            }
        }

        if (week > 0) {
            if (week % 10 == 0 | ((4 < (week % 10)) & ((week % 10) < 10)) | ((4 < week) & (week < 21))) {
                text_for_week = text_more_week;
            }// недель (5-20 Х5 Х6 Х7 Х8 Х9)
            else if (week % 10 == 1) {
                text_for_week = text_1_week;
            } // неделя
            else {
                text_for_week = text_week;
            }   // недели
            text_finish += week + text_for_week; // запихиваем в итоговую строку значение недель + правильное склонение
        }

        if (day > 0) {
            if (day % 7 == 0 | ((4 < (day % 7)) & ((day % 7) < 8)) | ((4 < day) & (day < 21))) {
                text_for_day = text_more_days;
            }// дней (5-20 Х5 Х6 Х7 Х8 Х9)
            else if (day % 10 == 1) {
                text_for_day = text_1_day;
            } // день
            else {
                text_for_day = text_days;
            }   // дня
            text_finish += " " + day + text_for_day;
        }

        if (hours > 0) {
            if (hours % 10 == 0 | ((4 < (hours % 10)) & ((hours % 10) < 10)) | ((4 < hours) & (hours < 21))) {
                text_for_hour = text_more_hours;
            }// часов (5-20 Х5 Х6 Х7 Х8 Х9)
            else if (hours % 10 == 1) {
                text_for_hour = text_1_hour;
            } // час
            else {
                text_for_hour = text_hours;
            }   // часа
            text_finish += " " + hours + text_for_hour;
        }

        if (minutes > 0) {
            if (minutes % 10 == 0 | ((4 < (minutes % 10)) & ((minutes % 10) < 10)) | ((4 < minutes) & (minutes < 21))) {
                text_for_min = text_more_minutes;
            }// минут (5-20 Х5 Х6 Х7 Х8 Х9)
            else if (minutes % 10 == 1) {
                text_for_min = text_1_min;
            } // минута
            else {
                text_for_min = text_minutes;
            }   // минуты
            text_finish += " " + minutes + text_for_min;
        }

        if (sec > 0) {
            if (sec % 10 == 0 | ((4 < (sec % 10)) & ((sec % 10) < 10)) | ((4 < sec) & (sec < 21))) {
                text_for_sec = text_more_sec;
            }// секунд (5-20 Х5 Х6 Х7 Х8 Х9)
            else if (sec % 10 == 1) {
                text_for_sec = text_1_sec;
            } // секунда
            else {
                text_for_sec = text_sec;
            }   // секунды
            text_finish += " " + sec + text_for_sec;
        }
        System.out.println(text_finish); // Выводим итоговую строчку в консоль

        // и переведём обратно дабы получить конкретное значение для проверки склонений

        Scanner in_1 = new Scanner(System.in);
        System.out.println("Введтите количество недель!");
        Scanner scanner_1 = new Scanner(System.in);
        int num_week = in_1.nextInt();

        Scanner in_2 = new Scanner(System.in);
        System.out.println("Введтите количество дней!");
        Scanner scanner_2 = new Scanner(System.in);
        int num_day = in_2.nextInt();

        Scanner in_3 = new Scanner(System.in);
        System.out.println("Введтите количество часов!");
        Scanner scanner_3 = new Scanner(System.in);
        int num_hour = in_2.nextInt();

        Scanner in_4 = new Scanner(System.in);
        System.out.println("Введтите количество минут!");
        Scanner scanner_4 = new Scanner(System.in);
        int num_min = in_4.nextInt();

        Scanner in_5 = new Scanner(System.in);
        System.out.println("Введтите количество секунд!");
        Scanner scanner_5 = new Scanner(System.in);
        int num_sec = in_5.nextInt();

        int x = num_week * 604800 + num_day * 86400 + num_hour * 3600 + (num_min * 60) + num_sec;
        System.out.println("Получилось  " + x + " секунд.");

    }
}