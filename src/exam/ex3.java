package exam;

import java.util.Scanner;

public class ex3 {
    public static void main(String[] args) {

        String happy_numbers = new String(); // загоняем сюда счастливые билеты
        String super_happy_numbers = new String();
        int counter = 0;  // счётчик количества счастливых билетов
        int counter_super = 0;  // суперсчастливые ( симметричные ) билеты

        // создаём 6 переменных для каждой цифры нашего номера билетика
        int a = 0; // 1-я цифра
        int b = 0; // 2-я цифра
        int c = 0; // 3-я цифра
        int d = 0; // 4-я цифра правая часть
        int e = 0; // 5-я цифра правая часть
        int f = 0; // 6-я цифра правая часть

        for (int i = 1000; i < 999999; i++) { // нет смысла считать билеты меньше, чем 1000
            int n = i; // дабы не сбивать счётчик i
            f = n % 10;   // начинаем определять каждую цифру числа
            e = ((n - f) / 10) % 10;
            n /= 10;
            d = ((n - e) / 10) % 10;
            n /= 10;
            c = ((n - d) / 10) % 10;
            n /= 10;
            if (n > 99) {
                b = ((n - c) / 10) % 10;
                n /= 10;
            }
            if (n > 9) {
                a = ((n - b) / 10) % 10;
            }
//для наглядной проверки правильности определения цифр билета: System.out.println (" a= " + a + " b = " + b + " c= " +c + " d= " +d + " e= " +e + " f= " +f);
            if ((a + b + c) == (d + e + f)) {
                counter++;
                if (i < 10000) {
                    happy_numbers += "00" + i + " ";
                } else if (i < 100000) {
                    happy_numbers += "0" + i + " ";
                } else {
                    happy_numbers += i + " ";
                }
            }

            if ((a == f) & (b == e) & (c == d)) { // определяем суперсчастливые числа
                counter_super++;
                if (i < 10000) {                  // для наглядности добавим перед числом недостающие нули
                    super_happy_numbers += "00" + i + " ";
                } else if (i < 100000) {
                    super_happy_numbers += "0" + i + " ";
                } else {
                    super_happy_numbers += i + " ";
                }
            }
        }
        System.out.println("Счастливых билетов = " + counter);
        System.out.println(happy_numbers);
        System.out.println("Счастливых билетов = " + counter);
        System.out.println("Суперсчастливых билетов = " + counter_super);

        //Если мы хотим увидеть отдельно суперсчастливые билеты
        Scanner in = new Scanner(System.in);
        System.out.println("Вывести список суперсчастливых билетов? ( Да - 1) ");
        Scanner scanner = new Scanner(System.in);
        int question = in.nextInt();

        if (question == 1) {
            System.out.println(super_happy_numbers);
        }
    }
}