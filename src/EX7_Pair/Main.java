package EX7_Pair;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Pair pair = new Pair();
        Scanner in_a = new Scanner(System.in);
        System.out.println("Введтите a ");
        Scanner scanner_a = new Scanner(System.in);
        pair.a = in_a.next();

        Scanner in_b = new Scanner(System.in);
        System.out.println("Введтите b ");
        Scanner scanner_b = new Scanner(System.in);
        pair.b = in_b.next();

        pair.first();
        pair.last();
        pair.swap();

        Scanner in_temp_a = new Scanner(System.in);
        System.out.println("Введтите новое значение 'a' для replaceFirst");
        Scanner scanner_temp_a = new Scanner(System.in);
        String temp_a = in_temp_a.next();

        pair.replaceFirst(temp_a);

        Scanner in_temp_b = new Scanner(System.in);
        System.out.println("Введтите новое значение 'b' для replaceLast");
        Scanner scanner_temp = new Scanner(System.in);
        String temp_b = in_temp_b.next();

        pair.replaceLast(temp_b);

        System.out.println("a = " + pair.a);
        System.out.println("b = " + pair.b);
    }
}