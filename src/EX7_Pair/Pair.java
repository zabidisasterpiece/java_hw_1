package EX7_Pair;

public class Pair {
    String a;
    String b;

    Pair() {
        a = a;
        b = b;
    }

    Pair(String a_S, String b_S) {
        a = a_S;
        b = b_S;
    }

    Pair(int a_I, int b_I) {
        String a_str = Integer.toString(a_I);
        String b_str = Integer.toString(b_I);
        a = a_str;
        b = b_str;
    }

    Pair(String a_S, int b_I) {
        String b_str = Integer.toString(b_I);
        a = a_S;
        b = b_str;
    }

    Pair(int a_I, String b_S) {
        String a_str = Integer.toString(a_I);
        a = a_str;
        b = b_S;
    }

    void first() {
        System.out.println("Метод 'first'. a = " + a);
    }

    void last() {
        System.out.println("Метод 'last' . b = " + b);
    }

    void swap() {
        String temp = a;
        a = b;
        b = temp;
        System.out.println("Метод swap. a = " + a + " . b = " + b);
    }

    void replaceFirst(String a_S) {
        a = a_S;
        System.out.println("Метод 'replaceFirst' . a = " + a);
    }

    void replaceFirst(int a_I) {
        String a_str = Integer.toString(a_I);
        a = a_str;
        System.out.println("Метод 'replaceFirst' . a = " + a);
    }

    void replaceLast(String b_S) {
        b = b_S;
        System.out.println("Метод 'replaceLast' . b = " + b);
    }

    void replaceLast(int b_I) {
        String b_str = Integer.toString(b_I);
        b = b_str;
        System.out.println("Метод 'replaceLast' . b = " + b);
    }
}